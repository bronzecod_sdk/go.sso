package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) UserSignup(name, email, password, signupIP string) (User, error) {
	var usr User
	d := marshal(map[string]interface{}{
		"User": email,
		"Pass": password,
		"Name": name,
		"IP":   signupIP,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/user/signup", c.proto, c.base), _json, nil, d, responseJSON, &usr)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("user", usr.ID.String()), &usr)
		c.cache.Store(strJoin("user-name", usr.Email), &usr)
	}
	return usr, err
}

func (c *connection) UserUpdate(id uuid.UUID, name, email string) error {
	d := marshal(map[string]interface{}{
		"id":    id,
		"name":  name,
		"email": email,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		var usr User
		if c.checkCache(strJoin("user", id.String()), &usr) {
			c.cache.Remove(strJoin("user", usr.ID.String()))
			c.cache.Remove(strJoin("user-name", usr.Email))
		}
	}
	return err
}

func (c *connection) UserBan(id uuid.UUID, state bool) error {
	d := marshal(map[string]interface{}{
		"id":    id,
		"state": state,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/ban", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		var usr User
		if c.checkCache(strJoin("user", id.String()), &usr) {
			c.cache.Remove(strJoin("user", usr.ID.String()))
			c.cache.Remove(strJoin("user-name", usr.Email))
		}
	}
	return err
}

func (c *connection) UserSetOrganisation(id uuid.UUID, orgID *uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"id":  id,
		"org": orgID,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/org", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		var usr User
		if c.checkCache(strJoin("user", id.String()), &usr) {
			c.cache.Remove(strJoin("user", usr.ID.String()))
			c.cache.Remove(strJoin("user-name", usr.Email))
		}
	}
	return err
}

func (c *connection) UserDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/user/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	if err == nil && c.cache != nil {
		var usr User
		if c.checkCache(strJoin("user", id.String()), &usr) {
			c.cache.Remove(strJoin("user", usr.ID.String()))
			c.cache.Remove(strJoin("user-name", usr.Email))
		}
	}
	return err
}

func (c *connection) UserTermsAccept(id uuid.UUID) error {
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/terms-accept?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) UserPasswordReset(id uuid.UUID) error {
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/pw-reset?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) UserReconfirm(id uuid.UUID) error {
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/reconfirm?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) UserUnlock(id uuid.UUID) error {
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/unlock?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) UserUpdateMetadata(id uuid.UUID, metadata map[string]string) error {
	d := marshal(map[string]interface{}{
		"id":       id,
		"metadata": metadata,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/metadata/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) UserMergeMetadata(id uuid.UUID, metadata map[string]string) error {
	d := marshal(map[string]interface{}{
		"id":       id,
		"metadata": metadata,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/user/metadata/merge", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}
