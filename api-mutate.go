package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) APICreate(email, name, password string, whitelist []APIWhitelistEntry, permissions map[string]string, product uuid.UUID) (APIUser, error) {
	var res APIUser
	d := marshal(map[string]interface{}{
		"email":       email,
		"name":        name,
		"password":    password,
		"whitelist":   whitelist,
		"permissions": permissions,
		"product":     product,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/apiuser/register", c.proto, c.base), _json, nil, d, responseJSON, &res)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("api", res.ID.String()), res)
	}
	return res, err
}

func (c *connection) APIUpdateDetails(id uuid.UUID, name, email string) error {
	d := marshal(map[string]interface{}{
		"id":    id,
		"name":  name,
		"email": email,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/apiuser/update/details", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		c.cache.Remove(strJoin("api", id.String()))
	}
	return err
}

func (c *connection) APIUpdatePermissions(id uuid.UUID, permissions map[string]string, merge bool) error {
	d := marshal(map[string]interface{}{
		"id":          id,
		"permissions": permissions,
		"merge":       merge,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/apiuser/update/access", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		c.cache.Remove(strJoin("api", id.String()))
	}
	return err
}

func (c *connection) APIDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/apiuser/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	if err == nil && c.cache != nil {
		c.cache.Remove(strJoin("api", id.String()))
	}
	return err
}
