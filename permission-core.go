package sso

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"
)

var (
	ErrQueryNoSubject = errors.New("must supply a source, target or product to query")
)

type PermissionSubject struct {
	ID           uuid.UUID      `json:"id"`
	CreatedAt    time.Time      `json:"createdAt"`
	UpdatedAt    time.Time      `json:"updatedAt"`
	Name         string         `json:"name"`
	Email        string         `json:"email"`
	Type         PermissionType `json:"type"`
	Platform     uuid.UUID      `json:"platform"`
	Organisation *uuid.UUID     `json:"organisation"`
}

// Permission is the access of a user/team to a given target
type Permission struct {
	ID        uint              `json:"id"`
	CreateAt  time.Time         `json:"createAt"`
	UpdatedAt time.Time         `json:"updatedAt"`
	DeletedAt *time.Time        `json:"deletedAt"`
	Product   uuid.UUID         `json:"product"`
	Title     string            `json:"title"`
	Source    uuid.UUID         `json:"source"`
	Target    uuid.UUID         `json:"target"`
	Access    map[string]string `json:"access"`
	Name      string            `json:"name,omitempty"` // name of the source team/user/api if known
	Type      PermissionType    `json:"type,omitempty"` // type of source, see below
}

// PermissionType is the type of entity that has access to the target
type PermissionType string

const (
	// PermissionTypeUnknown the source of the permission is not recorded in the platforms sso
	PermissionTypeUnknown PermissionType = "unknown"
	// PermissionTypeTeam the source of the permission is a group/team
	PermissionTypeTeam PermissionType = "team"
	// PermissionTypeUser the source of the permission is a user account
	PermissionTypeUser PermissionType = "user"
	// PermissionTypeAPI the source of the permission is an API account
	PermissionTypeAPI PermissionType = "api"
)

type PermissionEndpoints interface {

	// ======================
	// Get/Retrieve endpoints
	// ======================

	// PermissionSubjectsAvailable returns a list of subjects (teams, users, apis) that are available as permission
	// subjects in your platform (intended for use in administrative tools only, no scoping is provided)
	PermissionSubjectsAvailable(types []PermissionType, childPlatform *uuid.UUID) ([]PermissionSubject, error)

	// PermissionSubjectsGet returns a single subject (team, user or api) that has the corresponding ID in your platform
	PermissionSubjectsGet(id uuid.UUID) (PermissionSubject, error)

	// PermissionOwner gets the current owner permission for the provided target
	PermissionOwner(target uuid.UUID) (Permission, error)

	// PermissionLinks gets a list of all permissions that give source access to target
	PermissionLinks(source, target uuid.UUID, page *PageDetails) ([]Permission, error)

	// PermissionListForTarget gets a list of all permissions that target the identified target.
	// if a list of products is specified, only permissions of those products will be returned.
	// an empty array or nil will return all permissions for the target
	PermissionListForTarget(target uuid.UUID, products []uuid.UUID, page *PageDetails) ([]Permission, error)

	// PermissionListForSource gets a list of all permissions the provided source has.
	// if a product is provided it will limit the results to only permissions of that product type.
	// a nil product will return all permissions for the source
	PermissionListForSource(source uuid.UUID, product *uuid.UUID, page *PageDetails) ([]Permission, error)

	// PermissionQuery conducts a generic query of permissions within the callers platform
	PermissionQuery(query PermissionQuery) ([]Permission, error)

	// ======================
	//  Mutation endpoints
	// ======================

	// PermissionNew creates a new permission giving source access to target.
	// identifies the product this permission belongs to and sets the current label and access permissions
	PermissionNew(source, target, product uuid.UUID, label string, permissions map[string]string) (Permission, error)

	// PermissionNewBatch creates a list of new permissions for a group of sources to a given target
	PermissionNewBatch(sources []uuid.UUID, target, product uuid.UUID, label string, permissions map[string]string) (Permission, error)

	// PermissionRename renames all permissions for a given target to reflect a change in targets identifiable name
	PermissionRename(target uuid.UUID, label string) error

	// PermissionUpdate updates the identified permissions access rules
	// if merge is true, the provided list will keep existing rules, only overriding those defined here
	// if false, the permissions are set to the provided map
	// NOTE: this will never change the ownership flag
	PermissionUpdate(id uint, permission map[string]string, merge bool) error

	// PermissionUpdateForTarget will merge the provided permission map for all permissions to the target of the given product
	// NOTE: this will never change the ownership flag
	PermissionUpdateForTarget(target uuid.UUID, permission map[string]string, product uuid.UUID) error

	// PermissionTransferOwnership will transfer the current owner of target to recipient
	// NOTE: only one permission for a target may be owner at a time
	PermissionTransferOwnership(target, recipient uuid.UUID) error

	// PermissionDelete will remove a given permission and its associated access
	PermissionDelete(id uint) error

	// PermissionDeleteLinks will remove all direct permissions from source to target
	// NOTE: this will not alter indirect permissions such as a users access to target through a team they are a member of
	PermissionDeleteLinks(source, target uuid.UUID) error

	// PermissionDeleteTarget will delete all permissions referencing target, optionally restricted to the listed product families
	PermissionDeleteTarget(target uuid.UUID, products []uuid.UUID) error

	// PermissionDeleteBatch will delete all permissions in the provided list of IDs
	PermissionDeleteBatch(ids []uint) error
}
