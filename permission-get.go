package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) PermissionSubjectsAvailable(types []PermissionType, childPlatform *uuid.UUID) ([]PermissionSubject, error) {
	var (
		res      []PermissionSubject
		typesStr string
	)

	for i := range types {
		typesStr += string(types[i]) + "+"
	}
	if len(types) > 0 {
		typesStr = typesStr[:len(typesStr)-1]
	}
	url := fmt.Sprintf("%v://%v/userapi/permissions/subjects?types=%v", c.proto, c.base, typesStr)
	if childPlatform != nil {
		url += "&platform=" + childPlatform.String()
	}
	err := c.processRequest(http.MethodGet, url, _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) PermissionSubjectsGet(id uuid.UUID) (PermissionSubject, error) {
	var res PermissionSubject
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/permissions/subject?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) PermissionOwner(target uuid.UUID) (Permission, error) {
	var res Permission
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/permissions/owner?target=%v", c.proto, c.base, target), _json, nil, nil, responseJSON, &res)
	return res, err
}

// PageDetails wraps pagination requests that either limit the number of results returned to "Size" and/or offset the results to "Page" increments of size (starting at 0)
type PageDetails struct {
	Page int
	Size int
}

func (p *PageDetails) getPageString() string {
	str := p.getQueryPageString()
	if str != "" {
		str = "&" + str
	}
	return str
}

func (p *PageDetails) getQueryPageString() string {
	if p.Page == 0 && p.Size == 0 {
		return ""
	}
	if p.Page == 0 && p.Size > 0 {
		return fmt.Sprintf("limit=%d", p.Size)
	}
	return fmt.Sprintf("page=%v&limit=%d", p.Page, p.Size)
}

func (c *connection) PermissionLinks(source, target uuid.UUID, page *PageDetails) ([]Permission, error) {
	var (
		res     []Permission
		pageStr string
	)
	if page != nil {
		pageStr = page.getPageString()
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/permissions/links?source=%v&target=%v%v", c.proto, c.base, source, target, pageStr), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) PermissionListForTarget(target uuid.UUID, products []uuid.UUID, page *PageDetails) ([]Permission, error) {
	var (
		res     []Permission
		arg     string
		pageStr string
	)
	for _, id := range products {
		arg += id.String() + ","
	}
	if len(products) > 0 {
		arg = arg[:len(arg)-1]
	}

	if page != nil {
		pageStr = page.getPageString()
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/permissions/target?target=%v&products=%v%v", c.proto, c.base, target, arg, pageStr), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) PermissionListForSource(source uuid.UUID, product *uuid.UUID, page *PageDetails) ([]Permission, error) {
	var (
		res     []Permission
		pageStr string
	)
	if page != nil {
		pageStr = page.getPageString()
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/permissions/source?source=%v&product=%v%v", c.proto, c.base, source, product, pageStr), _json, nil, nil, responseJSON, &res)
	return res, err
}
