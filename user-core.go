package sso

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// User represents a set of account credentials for a human user of a platform
// and authentication for the platforms auth portal.
type User struct {
	ID           uuid.UUID         `json:"id"`
	CreatedAt    time.Time         `json:"createdAt"`
	UpdatedAt    time.Time         `json:"updatedAt"`
	API          uuid.UUID         `json:"api"`
	Email        string            `json:"email"`
	Name         string            `json:"name"`
	SignupIP     string            `json:"signupIP"`
	Admin        bool              `json:"admin"`
	Banned       bool              `json:"banned"`
	Confirmed    bool              `json:"confirmed"`
	Metadata     map[string]string `json:"metadata"`
	Organisation *uuid.UUID        `json:"organisation"`
}

type sessionUser struct {
	User
	Valid bool `json:"valid"`
}

type loginUser struct {
	User
	Cookie Session `json:"cookie"`
}

type UserEndpoints interface {
	// ======================
	// Get/Retrieve endpoints
	// ======================

	// UserExists checks if a user exists in the callers platform with the provided username
	UserExists(username string) (bool, error)

	// UserGet loads a user from the callers platform that has the provided ID (if one exists)
	UserGet(id uuid.UUID) (User, error)

	// UserGetUsername loads a user from the callers platform by their username (if one exists)
	UserGetUsername(username string) (User, error)

	// UserGetSession loads the user in the callers platform for which the provided cookie token applies (if valid)
	UserGetSession(cookie string) (bool, User, error)

	// UsersPlatform loads the list of all users on the callers platform
	UsersPlatform() ([]User, error)

	// ======================
	//  Mutation endpoints
	// ======================

	// UserSignup creates a new user with the provided details in the callers platform
	UserSignup(name, email, password, signupIP string) (User, error)

	// UserUpdate updates a platform user corresponding to the provided ID, updating their name and contact email
	// as well as those of all memberships that user has
	UserUpdate(id uuid.UUID, name, email string) error

	// UserBan updates a platform users banned status to the provided state
	UserBan(id uuid.UUID, state bool) error

	UserSetOrganisation(id uuid.UUID, orgID *uuid.UUID) error

	// UserDelete removes a user and all their memberships and permissions from the platform
	UserDelete(id uuid.UUID) error

	// UserTermsAccept adds a Terms and Conditions acceptance flag to the identified user for the latest configured terms
	UserTermsAccept(id uuid.UUID) error

	// UserPasswordReset sends a password reset request email to the identified platform user
	UserPasswordReset(id uuid.UUID) error

	// UserReconfirm sends an email confirmation email to the identified platform user, setting their confirmation status to false if it is not already
	UserReconfirm(id uuid.UUID) error

	// UserUnlock resets the "locked till" time on a user caused by too many failed login attempts
	UserUnlock(id uuid.UUID) error

	// UserUpdateMetadata sets the users metadata properties to the provided map
	UserUpdateMetadata(id uuid.UUID, metadata map[string]string) error

	// UserMergeMetadata merges the provided metadata map with the users metadata
	UserMergeMetadata(id uuid.UUID, metadata map[string]string) error
}
