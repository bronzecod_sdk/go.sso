package sso

import (
	"fmt"
	"net/http"
	"strings"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) TeamGet(id uuid.UUID) (Team, error) {
	var res Team
	if c.checkCache(strJoin("team", id.String()), &res) {
		return res, nil
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/teams/get?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("team", id.String()), res)
	}
	return res, err
}

func (c *connection) TeamPlatform() ([]Team, error) {
	var res []Team
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/teams/platform", c.proto, c.base), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) TeamListForUser(userID uuid.UUID) ([]Team, error) {
	var res []Team
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/teams/users?user=%v", c.proto, c.base, userID), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) TeamMemberExists(team, user uuid.UUID) (bool, error) {
	_, err := c.TeamMemberGet(team, user)
	if err != nil {
		if strings.Contains(err.Error(), "Member not found") {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (c *connection) TeamMemberGet(team, user uuid.UUID) (Membership, error) {
	var res Membership
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/teams/membership?team=%v&user=%v", c.proto, c.base, team, user), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) TeamMemberListForTeam(team uuid.UUID) ([]Membership, error) {
	var res []Membership
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/teams/members/list?id=%v", c.proto, c.base, team), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) TeamMemberListForUser(userID uuid.UUID) ([]Membership, error) {
	var res []Membership
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/teams/members/user?id=%v", c.proto, c.base, userID), _json, nil, nil, responseJSON, &res)
	return res, err
}
