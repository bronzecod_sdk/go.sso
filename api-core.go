package sso

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// APIUser represents a set of API credentials existing in a platform
type APIUser struct {
	ID           uuid.UUID           `json:"id"`
	CreatedAt    time.Time           `json:"createdAt"`
	UpdatedAt    time.Time           `json:"updatedAt"`
	Key          string              `json:"key"`
	Email        string              `json:"email"`
	Name         string              `json:"name"`
	Platform     uuid.UUID           `json:"platform"`
	Permission   string              `json:"permission"`
	Whitelist    []APIWhitelistEntry `json:"whitelist"`
	Organisation *uuid.UUID          `json:"organisation"`
}

// APIWhitelistEntry represents a single rule in an APIs whitelist
type APIWhitelistEntry struct {
	Label string               `json:"label"`
	Type  APIWhitelistRuleType `json:"type"`
	Rule  string               `json:"rule"`
}

// APIWhitelistRuleType is a string enumeration of supported rule types for API Whitelists
type APIWhitelistRuleType string

// list of API Whitelist Rule Types
const (
	// APIWhitelistTypeIP indicates a whitelist rule whitelists a single IP address
	APIWhitelistTypeIP APIWhitelistRuleType = "ipAddress"
	// APIWhitelistTypeRange indicates a whitelist rule whitelists a range of IP addresses
	APIWhitelistTypeRange APIWhitelistRuleType = "ipRange"
	// APIWhitelistTypeRegex indicates a whitelist rule allows all IP addresses that match the rules regular expression
	APIWhitelistTypeRegex APIWhitelistRuleType = "regex"
)

type APIEndpoints interface {
	// ======================
	// Get/Retrieve endpoints
	// ======================

	// APICheckCredentials attempts to get an API user by its provided Key
	// checking its credentials match, it has access to the provided service key
	// and the IP is valid in its whitelist.
	// it returns the API if the above conditions are met, or an error explaining the conditions failed
	APICheckCredentials(key, secret, ip, service string) (APIUser, error)

	// APIPlatformList returns a list of API users for the platform the requester is part of
	// on the condition the requesting API is one authorized to do so.
	// else it returns an error
	APIPlatformList() ([]APIUser, error)

	// APIGet will attempt to get an API by its canonical UUID, so long as it is part of the same platform as the requestor
	// and the requestor has permission to make the request.
	APIGet(id uuid.UUID) (APIUser, error)

	// APIList returns a list of platform APIs whose IDs are found in the provided list
	APIList(ids []uuid.UUID) ([]APIUser, error)

	// ======================
	//  Mutation endpoints
	// ======================

	// APICreate will create a new API user in the platform with the provided details
	// the APIs key is generated during creation and is found in the returned APIUser.
	APICreate(email, name, password string, whitelist []APIWhitelistEntry, permissions map[string]string, product uuid.UUID) (APIUser, error)

	// APIUpdateDetails updates the identified APIs details with the provided name and contact email
	APIUpdateDetails(id uuid.UUID, name, email string) error

	// APIUpdatePermissions updates the identified APIs access permission map with the provided set
	// if merge is true, the existing permissions will be merged with the provided set
	// otherwise it will replace the permissions entirely with those provided
	APIUpdatePermissions(id uuid.UUID, permissions map[string]string, merge bool) error

	// APIDelete removes an APIUser from the platform,
	// also deleting all owned by the API user, and all permissions pointing to it.
	// If the API user is owner of a target (has "Owner"=>"true" permission flag) this process will abort
	APIDelete(id uuid.UUID) error
}
