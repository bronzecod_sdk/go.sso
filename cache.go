package sso

import (
	"encoding/base64"
	"strings"
)

func (c *connection) checkCache(key string, val interface{}) bool {
	if c.cache == nil {
		return false
	}

	if c.cache.Has(key) {
		err := c.cache.Retrieve(key, val)
		if err != nil {
			return false
		}
	}

	return false
}

func b64Enc(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func strJoin(args ...string) string {
	return strings.Join(args, ":")
}
