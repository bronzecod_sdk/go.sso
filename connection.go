package sso

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/bronzecod_sdk/go.sdkUtils/bufferCache"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

// Connection to the Bronzecod SSO SDK
type connection struct {
	conn  http.Client
	base  string
	user  string
	pass  string
	proto string

	id        uuid.UUID
	platform  uuid.UUID
	host      string
	cookieKey string
	cache     bufferCache.BufferCache

	// special case for clients using nested platforms, contact sales
	childPlatforms map[string]testResponse
}

const (
	_json          = "application/json"
	responseNull   = "null"
	responseString = "string"
	responseJSON   = "json"
	responseBool   = "bool"
)

// Dial a new connection to a bronzecod sso instance
func Dial(baseurl, user, password string, tls *tls.Config) (Connection, error) {
	Conn := &connection{
		conn: http.Client{
			Timeout: time.Second * 5,
			Transport: &http.Transport{
				TLSClientConfig: tls,
			},
		},
		base: baseurl,
		user: user,
		pass: password,

		childPlatforms: map[string]testResponse{},
	}

	if tls != nil {
		Conn.proto = "https"
	} else {
		Conn.proto = "http"
		if strings.HasPrefix(baseurl, "https") {
			Conn.proto = "https"
		}
	}
	Conn.base = strings.TrimPrefix(Conn.base, "https://")
	Conn.base = strings.TrimPrefix(Conn.base, "http://")

	test, err := Conn.test(nil)
	if err != nil {
		return Conn, errors.Wrap(err, "Invalid credentials or invalid request source for API whitelist")
	}

	Conn.id = test.API
	Conn.platform = test.Platform
	Conn.host = test.Host
	Conn.cookieKey = test.CookieKey

	return Conn, nil
}

type testResponse struct {
	API       uuid.UUID
	Platform  uuid.UUID
	Host      string
	CookieKey string
}

func (c *connection) test(platform *uuid.UUID) (testResponse, error) {
	var res testResponse
	url := fmt.Sprintf("%v://%v/userapi/test", c.proto, c.base)
	if platform != nil {
		url += "?p=" + platform.String()
	}
	err := c.processRequest(http.MethodGet, url, _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) request(method, url, contentType string, headers map[string]string, body []byte) (*http.Response, error) {
	req, err := http.NewRequest(method, url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(c.user, c.pass)
	req.Header.Set("Content-Type", contentType)
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	return c.conn.Do(req)
}

func (c *connection) processRequest(method, url, contentType string, headers map[string]string, body []byte, expectedType string, response interface{}) error {
	resp, err := c.request(method, url, contentType, headers, body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < http.StatusOK || resp.StatusCode >= http.StatusMultipleChoices {
		var buf bytes.Buffer
		buf.ReadFrom(resp.Body)
		return errors.New(buf.String())
	}
	switch expectedType {
	case responseString:
		// HACK: short term work around to assign to strings
		var buf bytes.Buffer
		buf.ReadFrom(resp.Body)
		err = json.Unmarshal([]byte("\""+buf.String()+"\""), response)
		return err
	case responseJSON:
		err = json.NewDecoder(resp.Body).Decode(response)
		return err
	case responseBool:
		if resp.StatusCode == http.StatusOK {
			err = json.Unmarshal([]byte("true"), response)
		}
		return err
	case responseNull:
		return nil
	default:
		return errors.New("Unexpected response type")
	}
}

func marshal(data interface{}) []byte {
	resp, _ := json.Marshal(data)
	return resp
}

func (c *connection) SetCache(cache bufferCache.BufferCache) {
	c.cache = cache
}
