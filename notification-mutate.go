package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

// NotificationNew creates a new notification in the platform with the given details
// the source is optional for read/display, an empty target list will create a platform wide notification
// the remaining type, title, description and actions are used for rendering the notification in the user dashboard
func (c *connection) NotificationNew(source *uuid.UUID, targets []uuid.UUID, msgType, title, description string, actions []NotificationAction, platform *uuid.UUID) (Notification, error) {
	var res Notification
	d := marshal(map[string]interface{}{
		"source":      source,
		"target":      targets,
		"type":        msgType,
		"title":       title,
		"description": description,
		"actions":     actions,
		"platform":    platform,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/notification/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("notification", res.ID.String()), res)
	}
	return res, err
}

// NotificationDelete deletes the identified notification from the platform, removing it from all users visibility
func (c *connection) NotificationDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/notification/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}
