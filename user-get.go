package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) UserExists(username string) (bool, error) {
	var result bool
	if c.checkCache(strJoin("user-name", username), &User{}) {
		return true, nil
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/user/exists?e=%v", c.proto, c.base, username), _json, nil, nil, responseJSON, &result)
	return result, err
}

func (c *connection) UserGet(id uuid.UUID) (User, error) {
	var usr User
	if c.checkCache(strJoin("user", id.String()), &usr) {
		return usr, nil
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/user/get/id?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &usr)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("user", usr.ID.String()), &usr)
		c.cache.Store(strJoin("user-name", usr.Email), &usr)
	}
	return usr, err
}

func (c *connection) UserGetUsername(username string) (User, error) {
	var usr User
	if c.checkCache(strJoin("user-name", username), &usr) {
		return usr, nil
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/user/get/username?e=%v", c.proto, c.base, username), _json, nil, nil, responseJSON, &usr)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("user", usr.ID.String()), &usr)
		c.cache.Store(strJoin("user-name", usr.Email), &usr)
	}
	return usr, err
}

func (c *connection) UserGetSession(cookie string) (bool, User, error) {
	var usr sessionUser
	if c.checkCache(strJoin("user-cook", cookie), &usr) {
		return usr.Valid, usr.User, nil
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/user/get/session?t=%v", c.proto, c.base, cookie), _json, nil, nil, responseJSON, &usr)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("user", usr.ID.String()), &usr)
		c.cache.Store(strJoin("user-name", usr.Email), &usr)
		c.cache.Store(strJoin("user-cook", cookie), &usr)
	}
	return usr.Valid, usr.User, err
}

func (c *connection) UsersPlatform() ([]User, error) {
	var usr []User
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/user/platform", c.proto, c.base), _json, nil, nil, responseJSON, &usr)
	return usr, err
}
