package sso

import (
	"fmt"
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"
)

// Session represents a current user session, where key is the value stored in the cookie
type Session struct {
	Key       string    `json:"key"`
	CreatedAt time.Time `json:"createdAt"`
	Expires   time.Time `json:"expires"`
	Revoked   bool      `json:"revoked"`
	UserID    uuid.UUID `json:"userID"`
	Component string    `json:"component"`
}

func (c *connection) SessionLogin(username, password, component string) (User, Session, error) {
	var res loginUser
	d := marshal(map[string]interface{}{
		"username":  username,
		"password":  password,
		"component": component,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/session/login", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res.User, res.Cookie, err
}

func (c *connection) SessionLogout(key string) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/session/logout?key=%v", c.proto, c.base, key), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) SessionCheck(token string) (bool, error) {
	var res bool
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/session/check?t=%v", c.proto, c.base, token), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) SessionList(user uuid.UUID, component string) ([]Session, error) {
	var ls []Session
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/session/list?id=%v&component=%v", c.proto, c.base, user, component), _json, nil, nil, responseJSON, &ls)
	return ls, err
}

func (c *connection) SessionKey(platform *uuid.UUID) (string, error) {
	var res string
	token := ""
	if platform != nil {
		token = platform.String()
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/session/key?api=%v", c.proto, c.base, token), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) SessionRevokeOther(key, component string) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/session/revoke?key=%v&component=%v", c.proto, c.base, key, component), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) SessionExtend(key string) error {
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/session/extend?key=%v", c.proto, c.base, key), _json, nil, nil, responseNull, nil)
	return err
}

type SessionEndpoints interface {
	// ======================
	// Get/Retrieve endpoints
	// ======================

	// SessionList will list all sessions that correspond to the provided user ID
	// component will optionally restrict the returned sessions to that component, use an empty string ("") for all
	SessionList(user uuid.UUID, component string) ([]Session, error)

	// SessionCheck checks if the given session token is valid
	SessionCheck(token string) (bool, error)

	// SessionKey will get the current platforms session key (cookie key), or, if a parent platform,
	// the session key of one of it child platforms
	SessionKey(platform *uuid.UUID) (string, error)

	// ======================
	//  Mutation endpoints
	// ======================

	// SessionLogin creates a new session for a user given the provided credentials
	// component namespaces the session and represents the tool/platform the session exists for.
	// the default is "Web", common alternatives are "Mobile-App", "Desktop" and "Admin-Portal"
	//
	// The return values are
	// the user the credentials correspond to
	// the session key to assign to a cookie or other mechanism to identify the new session
	// any errors encountered processing the request
	SessionLogin(username, password, component string) (User, Session, error)

	// SessionLogout till terminate the session corresponding to the provided key, making it invalid for further authentication
	SessionLogout(key string) error

	// SessionRevokeOther will revoke all of a users sessions other than the one with the provided session key
	// component may be used to restrict that to a certain component, if an empty string, it will revoke sessions in all components
	SessionRevokeOther(key, component string) error

	// SessionExtend will extend the session with the provided key to the current time + the platforms configured extension time (see platform config)
	SessionExtend(key string) error

	// ======================
	//  migration endpoints
	// ======================

	// SessionMigrateRequest creates a session migration token and url.
	// to migrate a users session to the new domain, redirect them to the provided URL
	// after authenticating their account, they will be redirected to returnURL with the additional
	// query parameter `return` which should match token.
	// Pass the value of return to SessionMigrateResolve to retrieve the user and session details, so they
	// may be applied to the new domain
	SessionMigrateRequest(returnURL, component string) (token string, url string, err error)

	// SessionMigrateResolve will collect the session migration data corresponding to the provided token
	// to initiate a migrate request see SessionMigrateRequest
	SessionMigrateResolve(token string) (User, Session, error)
}
