# Bronzecod SSO SDK

[![Go Reference](https://pkg.go.dev/badge/bitbucket.org/bronzecod_sdk/go.sso.svg)](https://pkg.go.dev/bitbucket.org/bronzecod_sdk/go.sso)

Provides an interface to the bronzecod SSO server REST API for managing

 - User accounts
 - User Sessions
 - Teams and Members
 - API Accounts
 - Permission list and access
 - Organisation/enterprise entities

## Usage

To connect to the API, you will need your API account API Key, API Secret, the baseURL for your platforms deployment.  
With these, call `Dial` to create a new API connection and validate the credentials access.

```
    connection, err := sso.Dial("https://example.com/", apiKey, apiSecret, nil)
    if err != nil {
        // handle connection error
        // common causes:
        //   - incorrect baseURL
        //   - incorrect credentials
        //   - server running outside APIs configured whitelist
    }

    // use API connection here
```

If required, TLS configuration can be provided with the 4th argument.

## Documentation

Each method in the SDK wraps the corresponding API endpoint in the reference manual provided with your deployment.  
For specifics of each call, consult that manual.

Documentation on this specific package can be found in the Go reference documentation [![Go Reference](https://pkg.go.dev/badge/bitbucket.org/bronzecod_sdk/go.sso.svg)](https://pkg.go.dev/bitbucket.org/bronzecod_sdk/go.sso)
