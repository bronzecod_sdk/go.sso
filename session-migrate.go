package sso

import (
	"fmt"
	"net/http"
)

func (c *connection) SessionMigrateRequest(returnURL, component string) (token string, url string, err error) {
	var res struct {
		Token string `json:"token"`
		URL   string `json:"url"`
	}
	err = c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/session/migrate/request?ret=%v&com=%v", c.proto, c.base, returnURL, component), _json, nil, nil, responseJSON, &res)
	return res.Token, res.URL, err
}

func (c *connection) SessionMigrateResolve(token string) (User, Session, error) {
	var res loginUser
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/session/migrate/collect?t=%v", c.proto, c.base, token), _json, nil, nil, responseJSON, &res)
	return res.User, res.Cookie, err
}
