package sso

import (
	"net/http"

	"bitbucket.org/bronzecod_sdk/go.sdkUtils/bufferCache"
	uuid "github.com/satori/go.uuid"
)

// Connection is an instance of a connection to the bronzecod SSO server
// each function corresponds to a synchronous API call to the server
type Connection interface {
	APIEndpoints
	SessionEndpoints
	UserEndpoints
	TeamsEndpoints
	PermissionEndpoints
	OrganisationEndpoints
	NotificationEndpoints

	// =====================
	// utilities and helpers
	// =====================

	// SetCache sets a cache manager to allow api calls to be cached and reduce round trip times for common queries
	SetCache(cache bufferCache.BufferCache)
	// GetID gets the ID of the API user the SDK is connected as
	GetID() uuid.UUID
	// GetPlatformID gets the ID of the white label platform the API is connected as a part of
	GetPlatformID() uuid.UUID
	// GetHost gets the default hostname the platform is configured for
	GetHost() string
	// GetCookieKey gets the key the platform is configured for its session cookies to use
	GetCookieKey() string
	// UtilGetCookie gets the value of the cookie on the provided request (if it exists) that corresponds to a session
	// in this APIs platform
	UtilGetCookie(r *http.Request) string
	// UtilGetChildPlatformCookie gets the value of the cookie on the provided request (if it exists) that corresponds to
	// a session on the identified platform so long as that platform is either the current one, or one of its child platforms
	UtilGetChildPlatformCookie(r *http.Request, platform uuid.UUID) (string, error)
	// UtilSetSessionCookie sets a session cookie for this platform, equivalent to the user logging in through the dashboard
	UtilSetSessionCookie(w http.ResponseWriter, domain, path string, session Session)
}

var _ Connection = (*connection)(nil)
