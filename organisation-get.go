package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) OrganisationList() ([]Organisation, error) {
	var res []Organisation
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisations", c.proto, c.base), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationsUserManages(userID uuid.UUID) ([]Organisation, error) {
	var res []Organisation
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/managed?user=%v", c.proto, c.base, userID), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationsUserMember(userID uuid.UUID) ([]Organisation, error) {
	var res []Organisation
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/user?user=%v", c.proto, c.base, userID), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationGet(id uuid.UUID) (Organisation, error) {
	var res Organisation
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationGetManagers(orgID uuid.UUID) ([]Membership, error) {
	var res []Membership
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/managers?id=%v", c.proto, c.base, orgID), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationGetUsers(orgID uuid.UUID) ([]User, error) {
	var res []User
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/users?id=%v", c.proto, c.base, orgID), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationGetTeams(orgID uuid.UUID) ([]Team, error) {
	var res []Team
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/teams?id=%v", c.proto, c.base, orgID), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) OrganisationHasManager(orgID, userID uuid.UUID) (bool, error) {
	var res bool
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/manager/exists?org=%v&user=%v", c.proto, c.base, orgID, userID), _json, nil, nil, responseBool, &res)
	return res, err
}
func (c *connection) OrganisationManagerToEntity(managerID, targetID uuid.UUID) (bool, error) {
	var res bool
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/organisation/manager/target?user=%v&target=%v", c.proto, c.base, managerID, targetID), _json, nil, nil, responseBool, &res)
	return res, err
}
