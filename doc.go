// Package sso is a golang SDK for the Bronzecod SSO platforms REST API
//
// it provides type safe bindings for the APIs core features and entities
// as well as the various API endpoints available to get, enumerate and mutate them.
package sso
