package sso

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) PermissionNew(source, target, product uuid.UUID, label string, permissions map[string]string) (Permission, error) {
	var res Permission
	d := marshal(map[string]interface{}{
		"source":      source,
		"target":      target,
		"product":     product,
		"permissions": permissions,
		"label":       label,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/permissions/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) PermissionNewBatch(sources []uuid.UUID, target, product uuid.UUID, label string, permissions map[string]string) (Permission, error) {
	var res Permission
	d := marshal(map[string]interface{}{
		"sources":     sources,
		"target":      target,
		"product":     product,
		"permissions": permissions,
		"label":       label,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/permissions/batch/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) PermissionRename(target uuid.UUID, label string) error {
	d := marshal(map[string]interface{}{
		"target": target,
		"label":  label,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/permissions/rename", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) PermissionUpdate(id uint, permission map[string]string, merge bool) error {
	d := marshal(map[string]interface{}{
		"id":         id,
		"permission": permission,
		"merge":      merge,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/permissions/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) PermissionUpdateForTarget(target uuid.UUID, permission map[string]string, product uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"target":     target,
		"permission": permission,
		"product":    product,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/permissions/batch/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) PermissionTransferOwnership(target, recipient uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"target":      target,
		"destination": recipient,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/permissions/transfer", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) PermissionDelete(id uint) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/permissions/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) PermissionDeleteLinks(source, target uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/permissions/delete/links?source=%v&target=%v", c.proto, c.base, source, target), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) PermissionDeleteTarget(target uuid.UUID, products []uuid.UUID) error {
	prods := make([]string, len(products))
	for i, id := range products {
		prods[i] = id.String()
	}
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/permissions/delete/target?target=%v&products=%v", c.proto, c.base, target, strings.Join(prods, "+")), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) PermissionDeleteBatch(ids []uint) error {
	var arg string
	for _, id := range ids {
		arg += strconv.FormatUint(uint64(id), 10) + ","
	}
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/permissions/batch/delete?ids=%v", c.proto, c.base, arg[len(arg)-1]), _json, nil, nil, responseNull, nil)
	return err
}
