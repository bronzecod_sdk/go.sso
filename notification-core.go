package sso

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Notification represents a notification visible to a user in their dashboard
// containing details for visualization in the dashboard and who it is visible too
type Notification struct {
	ID          uuid.UUID            `json:"id"`
	CreatedAt   time.Time            `json:"createdAt"`
	UpdatedAt   time.Time            `json:"updatedAt"`
	DeletedAt   *time.Time           `json:"deletedAt"`
	Platform    uuid.UUID            `json:"platform"`
	Source      *uuid.UUID           `json:"source"`
	Target      []uuid.UUID          `json:"target"`
	Type        string               `json:"type"`
	Title       string               `json:"title"`
	Description string               `json:"description"`
	Actions     []NotificationAction `json:"actions"`
}

// NotificationAction define action buttons presented as part of a notification in the dashboard
type NotificationAction struct {
	Type  string `json:"type"`
	Label string `json:"label"`
	Event string `json:"event"`
}

type NotificationEndpoints interface {

	// ======================
	//  Mutation endpoints
	// ======================

	// NotificationNew creates a new notification in the platform with the given details
	// the source is optional for read/display, an empty target list will create a platform wide notification
	// the remaining type, title, description and actions are used for rendering the notification in the user dashboard
	NotificationNew(source *uuid.UUID, targets []uuid.UUID, msgType, title, description string, actions []NotificationAction, platform *uuid.UUID) (Notification, error)

	// NotificationDelete deletes the identified notification from the platform, removing it from all users visibility
	NotificationDelete(id uuid.UUID) error
}
