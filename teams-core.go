package sso

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Team represents a group of users/apis that have access to entities as a group
type Team struct {
	ID           uuid.UUID    `json:"id"`
	Name         string       `json:"name"`
	Admin        *bool        `json:"admin,omitempty"`
	Members      []Membership `json:"members"`
	Organisation *uuid.UUID   `json:"organisation"`
}

// Membership respresents a users access to a team
type Membership struct {
	CreateAt  time.Time  `json:"createAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `json:"deletedAt"`
	Team      uuid.UUID  `json:"team"`
	User      uuid.UUID  `json:"user"`
	Label     string     `json:"label"`
	Email     string     `json:"email"`
	Admin     bool       `json:"admin"`
}

type TeamsEndpoints interface {
	// ======================
	// Get/Retrieve endpoints
	// ======================

	// TeamGet will load a team from the platform with the given ID
	TeamGet(id uuid.UUID) (Team, error)

	// TeamPlatform will load all teams in the current platform
	TeamPlatform() ([]Team, error)

	// TeamListForUser will list all teams the identified user has access too
	TeamListForUser(userID uuid.UUID) ([]Team, error)

	// TeamMemberExists identifies if a given User is a member of the given Team
	TeamMemberExists(team, user uuid.UUID) (bool, error)

	// TeamMemberGet retrieves a users membership to a team
	TeamMemberGet(team, user uuid.UUID) (Membership, error)

	// TeamMemberListForTeam lists all memberships for the identified team
	TeamMemberListForTeam(team uuid.UUID) ([]Membership, error)

	// TeamMemberListForUser lists all memberships a given user has
	TeamMemberListForUser(userID uuid.UUID) ([]Membership, error)

	// ======================
	//  Mutation endpoints
	// ======================

	// TeamNew creates a new team in the platform with the given name
	// creating it first user (and admin) as initialUser
	TeamNew(name string, initialUser uuid.UUID) (Team, error)

	// TeamRename renames the identified team to the provided new name
	TeamRename(id uuid.UUID, newName string) error

	TeamSetOrganisation(id uuid.UUID, orgID *uuid.UUID) error

	// TeamDelete removes a team from the platform, removing all its memberships and permissions
	TeamDelete(id uuid.UUID) error

	// TeamMemberAdd adds a new member to the team (not as an admin),
	// member should be a User or APIUser
	TeamMemberAdd(team, member uuid.UUID) (Membership, error)

	// TeamMemberUpdateLabels will update the display name and contact email for a membership
	//
	// NOTE: this is a fallback for legacy platforms, Users and APIUsers will automatically update
	// their corresponding memberships when their details are updated.
	TeamMemberUpdateLabels(team, member uuid.UUID, name, email string) error

	// TeamMemberSetAdmin will set the team members admin status within the team
	TeamMemberSetAdmin(team, member uuid.UUID, admin bool) error

	// TeamMemberRemove will remove the given member from the team
	TeamMemberRemove(team, member uuid.UUID) error
}
