package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) TeamNew(name string, initialUser uuid.UUID) (Team, error) {
	var res Team
	d := marshal(map[string]interface{}{
		"name": name,
		"user": initialUser,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/teams/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("team", res.ID.String()), res)
	}
	return res, err
}

func (c *connection) TeamRename(id uuid.UUID, newName string) error {
	d := marshal(map[string]interface{}{
		"team": id,
		"name": newName,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/teams/rename", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		c.cache.Remove(strJoin("team", id.String()))
	}
	return err
}

func (c *connection) TeamSetOrganisation(id uuid.UUID, orgID *uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"team": id,
		"org":  orgID,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/teams/org", c.proto, c.base), _json, nil, d, responseNull, nil)
	if err == nil && c.cache != nil {
		c.cache.Remove(strJoin("team", id.String()))
	}
	return err
}

func (c *connection) TeamDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/teams/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	if err == nil && c.cache != nil {
		c.cache.Remove(strJoin("team", id.String()))
	}
	return err
}

func (c *connection) TeamMemberAdd(team, member uuid.UUID) (Membership, error) {
	var res Membership
	d := marshal(map[string]interface{}{
		"team": team,
		"user": member,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/teams/member/add", c.proto, c.base), _json, nil, d, responseJSON, &res)
	return res, err
}

func (c *connection) TeamMemberUpdateLabels(team, member uuid.UUID, name, email string) error {
	d := marshal(map[string]interface{}{
		"team":   team,
		"member": member,
		"name":   name,
		"email":  email,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/teams/member/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) TeamMemberSetAdmin(team, member uuid.UUID, admin bool) error {
	d := marshal(map[string]interface{}{
		"team":   team,
		"member": member,
		"admin":  admin,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/teams/member/promote", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) TeamMemberRemove(team, member uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/teams/member/remove?team=%v&member=%v", c.proto, c.base, team, member), _json, nil, nil, responseNull, nil)
	return err
}
