package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) OrganisationNew(name, logo string, userLimit, teamLimit, permissionLimit int, teamIDs []uuid.UUID) (Organisation, error) {
	var res Organisation
	d := marshal(map[string]interface{}{
		"name":            name,
		"logo":            logo,
		"userLimit":       userLimit,
		"teamLimit":       teamLimit,
		"permissionLimit": permissionLimit,
		"teams":           teamIDs,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/organisation/new", c.proto, c.base), _json, nil, d, responseJSON, &res)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("org", res.ID.String()), res)
	}
	return res, err
}

func (c *connection) OrganisationUpdateDetails(id uuid.UUID, name, logo string) error {
	d := marshal(map[string]interface{}{
		"id":   id,
		"name": name,
		"logo": logo,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/organisation/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationUpdateConstraints(id uuid.UUID, userLimit, teamLimit, permissionLimit int) error {
	d := marshal(map[string]interface{}{
		"id":              id,
		"userLimit":       userLimit,
		"teamLimit":       teamLimit,
		"permissionLimit": permissionLimit,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/organisation/constrain", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationDelete(id uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/organisation/delete?id=%v", c.proto, c.base, id), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) OrganisationTeamAddExisting(orgID, teamID uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"organisation": orgID,
		"team":         teamID,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/organisation/team/add", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationTeamAddNew(orgID uuid.UUID, teamName string, firstMember uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"organisation": orgID,
		"name":         teamName,
		"member":       firstMember,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/organisation/team/add", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationTeamRemove(orgID, teamID uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/organisation/team/remove?id=%v&team=%v", c.proto, c.base, orgID, teamID), _json, nil, nil, responseNull, nil)
	return err
}

func (c *connection) OrganisationManagerAdd(orgID, userID uuid.UUID) error {
	d := marshal(map[string]interface{}{
		"organisation": orgID,
		"manager":      userID,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/organisation/managers/add", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationManagerUpdateLabels(team, member uuid.UUID, name, email string) error {
	d := marshal(map[string]interface{}{
		"team":   team,
		"member": member,
		"name":   name,
		"email":  email,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/teams/member/update", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationManagerSetAdmin(team, member uuid.UUID, admin bool) error {
	d := marshal(map[string]interface{}{
		"team":   team,
		"member": member,
		"admin":  admin,
	})
	err := c.processRequest(http.MethodPut, fmt.Sprintf("%v://%v/userapi/teams/member/promote", c.proto, c.base), _json, nil, d, responseNull, nil)
	return err
}

func (c *connection) OrganisationManagerRemove(team, member uuid.UUID) error {
	err := c.processRequest(http.MethodDelete, fmt.Sprintf("%v://%v/userapi/teams/member/remove?team=%v&member=%v", c.proto, c.base, team, member), _json, nil, nil, responseNull, nil)
	return err
}
