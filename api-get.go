package sso

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func (c *connection) APICheckCredentials(key, secret, ip, service string) (APIUser, error) {
	var res APIUser
	if c.checkCache(strJoin("api-cred", b64Enc(strJoin(key, secret)), ip), &res) {
		return res, nil
	}
	d := marshal(map[string]interface{}{
		"Key":     key,
		"Secret":  secret,
		"IP":      ip,
		"Service": service,
	})
	err := c.processRequest(http.MethodPost, fmt.Sprintf("%v://%v/userapi/apiuser/check", c.proto, c.base), _json, nil, d, responseJSON, &res)
	if err != nil {
		return res, err
	}
	if c.cache != nil {
		c.cache.Store(strJoin("api-cred", b64Enc(strJoin(key, secret)), ip), &res)
		c.cache.Store(strJoin("api", res.ID.String()), &res)
	}

	return res, nil
}

func (c *connection) APIPlatformList() ([]APIUser, error) {
	var res []APIUser
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/apiuser/platform", c.proto, c.base), _json, nil, nil, responseJSON, &res)
	return res, err
}

func (c *connection) APIGet(id uuid.UUID) (APIUser, error) {
	var res APIUser
	if c.checkCache(strJoin("api", id.String()), &res) {
		return res, nil
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/apiuser/get?id=%v", c.proto, c.base, id), _json, nil, nil, responseJSON, &res)
	if err == nil && c.cache != nil {
		c.cache.Store(strJoin("api", id.String()), res)
	}
	return res, err
}

func (c *connection) APIList(ids []uuid.UUID) ([]APIUser, error) {
	var res []APIUser
	var arg string
	for _, id := range ids {
		arg += id.String() + ","
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v/userapi/apiuser/list?id=%v", c.proto, c.base, arg[:len(arg)-1]), _json, nil, nil, responseJSON, &res)
	if err == nil && c.cache != nil {
		for _, api := range res {
			c.cache.Store(strJoin("api", api.ID.String()), api)
		}
	}
	return res, err
}
