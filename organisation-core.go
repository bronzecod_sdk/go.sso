package sso

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// Organisation represents a user facing organisational unit, used for building enterprise entities in consumer
// facing dashboards.
// An organisation consists of public facing identifiers (name and logo) and a set of constraints on the number of
// users, teams and permissions that may exist as members of an organisation (a value of 0 denotes no limitation).
//
// Teams are the base unit attached to an organisation and are limited directly
// Users are counted as unique users that are members of those teams, and
// Permissions are counted as permissions to unique targets from those teams
type Organisation struct {
	ID              uuid.UUID `json:"id"`
	CreatedAt       time.Time `json:"createdAt"`
	UpdatedAt       time.Time `json:"updatedAt"`
	Platform        uuid.UUID `json:"platform"`
	Name            string    `json:"name"`
	Logo            string    `json:"logo"`
	UserLimit       int       `json:"userLimit"`
	TeamLimit       int       `json:"teamLimit"`
	PermissionLimit int       `json:"permissionLimit"`
}

type OrganisationEndpoints interface {

	// ======================
	// Get/Retrieve endpoints
	// ======================

	// OrganisationList will load all organisations in the platform
	OrganisationList() ([]Organisation, error)

	// OrganisationsUserManages will load all organisations the identified user is a manager of
	OrganisationsUserManages(userID uuid.UUID) ([]Organisation, error)

	// OrganisationsUserMember will load all organisations the identified user is a member of
	OrganisationsUserMember(userID uuid.UUID) ([]Organisation, error)

	// OrganisationGet will load the identified organisation from the platform
	OrganisationGet(id uuid.UUID) (Organisation, error)

	// OrganisationGetManagers will load all the memberships identifying the managers of the identified organisation
	OrganisationGetManagers(orgID uuid.UUID) ([]Membership, error)

	// OrganisationGetUsers will load all users who are members of the identified organisation
	OrganisationGetUsers(orgID uuid.UUID) ([]User, error)

	// OrganisationGetTeams will load all teams making a part of the organisation
	OrganisationGetTeams(orgID uuid.UUID) ([]Team, error)

	// OrganisationHasManager checks if the identified user is a manager of the identified organisation
	OrganisationHasManager(orgID, userID uuid.UUID) (bool, error)

	// OrganisationManagerToEntity checks if the identified manager, manages the organisation containing the user,api or team targeted
	OrganisationManagerToEntity(managerID, targetID uuid.UUID) (bool, error)

	// ======================
	//  Mutation endpoints
	// ======================

	// OrganisationNew creates a new organisation in the platform with the given name logo and constraints
	// any teams whose ID are in the provided list will be added to the organisation
	// (unless they are part of another organisation, which will generate an error)
	OrganisationNew(name, logo string, userLimit, teamLimit, permissionLimit int, teamIDs []uuid.UUID) (Organisation, error)

	// OrganisationUpdateDetails will update the identified organisation's name and logo
	OrganisationUpdateDetails(id uuid.UUID, name, logo string) error

	// OrganisationUpdateConstraints will update the identified organisations constraint limits on the number of users,
	// teams and permissions it may have
	OrganisationUpdateConstraints(id uuid.UUID, userLimit, teamLimit, permissionLimit int) error

	// OrganisationDelete will delete the identified organisation, removing its teams from it, but without deleting them
	OrganisationDelete(id uuid.UUID) error

	// OrganisationTeamAddExisting will add an existing team to the identified organisation
	OrganisationTeamAddExisting(orgID, teamID uuid.UUID) error

	// OrganisationTeamAddNew will create a new team in the organisation with the provided name and first admin member
	OrganisationTeamAddNew(orgID uuid.UUID, teamName string, firstMember uuid.UUID) error

	// OrganisationTeamRemove will remove the identified team from the identified organisation
	OrganisationTeamRemove(orgID, teamID uuid.UUID) error

	// OrganisationManagerAdd will add the specified user as a manager of the organisation
	OrganisationManagerAdd(orgID, userID uuid.UUID) error

	OrganisationManagerUpdateLabels(orgID, manager uuid.UUID, name, email string) error

	// OrganisationManagerSetAdmin will change the identified manager of an organisations status as an administrator
	// of that organisation to the provided value
	OrganisationManagerSetAdmin(orgID, manager uuid.UUID, admin bool) error

	// OrganisationManagerRemove will remove the provided user as a manager of the organisation
	OrganisationManagerRemove(orgID, user uuid.UUID) error
}
