package sso

import (
	"net/http"
	"time"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

// GetID returns the configured request credentials API User ID
func (c *connection) GetID() uuid.UUID {
	return c.id
}

// GetPlatformID gets the ID of the platform the request credentials belong to
func (c *connection) GetPlatformID() uuid.UUID {
	return c.platform
}

// GetHost returns the default host for the platform the request credentials belong to
func (c *connection) GetHost() string {
	return c.host
}

// GetCookieKey gets the cookie key used for this platform
func (c *connection) GetCookieKey() string {
	return c.cookieKey
}

// UtilGetCookie attempts to get the platforms session cookie from a request, returning an empty string if none is found
func (c *connection) UtilGetCookie(r *http.Request) string {
	for _, cookie := range r.Cookies() {
		if cookie.Name == c.cookieKey {
			return cookie.Value
		}
	}
	return ""
}

func (c *connection) UtilGetChildPlatformCookie(r *http.Request, platform uuid.UUID) (string, error) {
	if uuid.Equal(platform, c.platform) {
		return c.UtilGetCookie(r), nil
	}
	plat, ok := c.childPlatforms[platform.String()]
	if !ok {
		var err error
		plat, err = c.test(&platform)
		if err != nil {
			return "", errors.Wrap(err, "error reading child platforms client details")
		}
		c.childPlatforms[plat.Platform.String()] = plat
	}

	for _, cookie := range r.Cookies() {
		if cookie.Name == plat.CookieKey {
			return cookie.Value, nil
		}
	}
	return "", nil
}

func newCookie(name, value, path, domain string, age int64, secure bool) *http.Cookie {
	var utctime time.Time
	if age == 0 {
		// 2^31 - 1 seconds (roughly 2038)
		utctime = time.Unix(2147483647, 0)
	} else {
		utctime = time.Unix(time.Now().Unix()+age, 0)
	}
	return &http.Cookie{
		Name:     name,
		Value:    value,
		Expires:  utctime,
		Path:     path,
		Domain:   domain,
		Secure:   secure,
		HttpOnly: true,
	}
}

func setHeader(w http.ResponseWriter, hdr string, val string, unique bool) {
	if unique {
		w.Header().Set(hdr, val)
	} else {
		w.Header().Add(hdr, val)
	}
}

// UtilSetSessionCookie manages setting a given session as a cookie for client persistence
func (c *connection) UtilSetSessionCookie(w http.ResponseWriter, domain, path string, session Session) {
	if domain == "" {
		domain = c.host
	}
	if path == "" {
		path = "/"
	}
	setHeader(w, "Set-Cookie", newCookie(c.cookieKey, session.Key, path, domain, session.Expires.Unix()-time.Now().Unix(), true).String(), false)
}
