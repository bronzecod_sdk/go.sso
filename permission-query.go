package sso

import (
	"fmt"
	"net/http"
	"strings"

	uuid "github.com/satori/go.uuid"
)

// PermissionQuery wraps the details to query platform permissions by
// source, target or product, with further optional query by access keys
// subject to different conditions
//
// NOTE: permission conditions behave as a logical OR with each other
// i.e. "Owner" equals "true" OR "Admin" equals "true" OR "team" has token "edit-members"
type PermissionQuery struct {
	source      *uuid.UUID
	target      *uuid.UUID
	product     *uuid.UUID
	permissions map[string][2]string
	page        PageDetails
}

const (
	// permQueryKeyPresent checks if a given key is pressent in a permissions access list
	permQueryKeyPresent = "keyPresent"
	// permQueryHasToken checks if the given keys value contains the provided value as an independent token (value surrounded either by the end of the value, whitespace or wordbreak)
	permQueryHasToken = "hasToken"
	// permQueryPartialToken checks if the given keys value contains the provided value in any form
	permQueryPartialToken = "partialToken"
	// permQueryEqual checks if the given keys value is exactly equal to the provided value
	permQueryEqual = "equal"
)

func (q *PermissionQuery) getURL() string {
	url := "/userapi/permissions/query?"
	args := make([]string, 0, 4)
	if q.source != nil {
		args = append(args, "source="+q.source.String())
	}
	if q.target != nil {
		args = append(args, "target="+q.target.String())
	}
	if q.product != nil {
		args = append(args, "product="+q.product.String())
	}
	if len(q.permissions) > 0 {
		items := make([]string, len(q.permissions))
		i := 0
		for key, args := range q.permissions {
			items[i] = fmt.Sprintf("%v|%v|%v", key, args[0], args[1])
			i++
		}
		args = append(args, strings.Join(args, ","))
	}
	pageStr := q.page.getQueryPageString()
	if pageStr != "" {
		args = append(args, pageStr)
	}

	return url + strings.Join(args, "&")
}

// PermissionQueryBySource creates a new query that will return permissions with the given source
func PermissionQueryBySource(id uuid.UUID) *PermissionQuery {
	return &PermissionQuery{source: &id}
}

// Source sets the query to find permissions with the given source
func (q *PermissionQuery) Source(id uuid.UUID) *PermissionQuery {
	q.source = &id
	return q
}

// PermissionQueryByTarget creates a new query that will return permissions with the given target
func PermissionQueryByTarget(id uuid.UUID) *PermissionQuery {
	return &PermissionQuery{target: &id}
}

// Target sets the query to find permissions with the given target
func (q *PermissionQuery) Target(id uuid.UUID) *PermissionQuery {
	q.target = &id
	return q
}

// PermissionQueryByProduct creates a new query that will return permissions with the given product
func PermissionQueryByProduct(id uuid.UUID) *PermissionQuery {
	return &PermissionQuery{product: &id}
}

// Product sets the query to only find permissions of the given product
func (q *PermissionQuery) Product(id uuid.UUID) *PermissionQuery {
	q.product = &id
	return q
}

// AccessPresent queries for permissions that have the provided permission key present in any form
func (q *PermissionQuery) AccessPresent(key string) *PermissionQuery {
	q.permissions[key] = [2]string{permQueryKeyPresent, ""}
	return q
}

// AccessHas queries for permissions that have a given token in the given access key
func (q *PermissionQuery) AccessHas(key string, value string) *PermissionQuery {
	q.permissions[key] = [2]string{permQueryHasToken, value}
	return q
}

// AccessLike queries for permissions that have a matching token (or partial token) in the given access key
func (q *PermissionQuery) AccessLike(key string, value string) *PermissionQuery {
	q.permissions[key] = [2]string{permQueryPartialToken, value}
	return q
}

// AccessEqual queries for permissions who's permissions under the given key exactly matches the provided value
func (q *PermissionQuery) AccessEqual(key string, value string) *PermissionQuery {
	q.permissions[key] = [2]string{permQueryEqual, value}
	return q
}

func (q *PermissionQuery) Limit(limit int) *PermissionQuery {
	q.page.Size = limit
	return q
}

func (q *PermissionQuery) Page(page int) *PermissionQuery {
	q.page.Page = page
	return q
}

// IsValid returns whether the provided query is a valid query that can be executed
func (q *PermissionQuery) IsValid() bool {
	return q.source != nil || q.target != nil || q.product != nil
}

func (c *connection) PermissionQuery(query PermissionQuery) ([]Permission, error) {
	var res []Permission
	if !query.IsValid() {
		return res, ErrQueryNoSubject
	}
	err := c.processRequest(http.MethodGet, fmt.Sprintf("%v://%v%v", c.proto, c.base, query.getURL()), _json, nil, nil, responseJSON, &res)
	return res, err
}
